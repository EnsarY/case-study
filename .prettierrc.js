module.exports = {
  semi: true,                  // Satır sonlarında noktalı virgül kullan
  trailingComma: 'es5',        // ES5 uyumlu trailing comma (örneğin, fonksiyon parametrelerinden sonra değil)
  singleQuote: true,           // Tek tırnak kullanımı
  printWidth: 80,              // Satır uzunluğu maksimum 80 karakter
  tabWidth: 2,                 // Tab genişliği olarak 2 boşluk kullan
  useTabs: false,              // Tab yerine boşluk kullan
  bracketSpacing: true,        // Nesne parantezleri arasında boşluk bırak (örn: { foo: bar })
  arrowParens: 'always',       // Ok fonksiyonlarında her zaman parantez kullan (örn: (x) => x)
  jsxBracketSameLine: false,   // JSX'te kapanış etiketini yeni satıra koy
  endOfLine: 'auto',           // Satır sonlarını işletim sistemine göre ayarla
};


