import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Button } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart } from '../slices/cartSlice';
import { addToFavorites, removeFromFavorites } from '../slices/favoritesSlice';
import { showMessage } from "react-native-flash-message";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const ListItem = ({ item }) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const favorites = useSelector((state) => state.favorites.items);
    const isFavorite = favorites.some((favorite) => favorite.id === item.id);

    const handleAddToCart = () => {
        dispatch(addToCart(item));
        showMessage({
            message: "Product added to cart",
            type: "success",
            position: 'center',
        });
    };

    const handleFavoriteToggle = () => {
        if (isFavorite) {
            dispatch(removeFromFavorites(item));
        } else {
            dispatch(addToFavorites(item));
        }
    };

    const truncatedDescription = item.description.length > 100
        ? item.description.slice(0, 100) + '...'
        : item.description;


    return (
        <View style={styles.itemContainer}>
            <TouchableOpacity onPress={handleFavoriteToggle} style={styles.favoriteIcon}>
                <MaterialCommunityIcons
                    name={isFavorite ? "star" : "star-outline"}
                    size={24}
                    color="#ffd700"
                />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Detail", { data: item })} style={styles.touchableArea}>
                <Image resizeMode="contain" source={{ uri: item.image }} style={styles.itemImage} />
                <View style={styles.itemDetails}>
                    <Text style={styles.itemName}>{item.name}</Text>
                    <Text style={styles.itemDescription}>{truncatedDescription}</Text>
                    <Text style={styles.itemPrice}>${item.price}</Text>
                </View>
            </TouchableOpacity>
            <Button icon="cart" mode="contained" onPress={handleAddToCart} style={styles.addButton}>
                Add to Cart
            </Button>
        </View>
    );
};

const styles = StyleSheet.create({
    itemContainer: {
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        flex: 1,
        justifyContent: 'space-between',
    },
    touchableArea: {
        flex: 1,
    },
    itemImage: {
        width: '100%',
        height: 200,
        borderRadius: 5,
        marginRight: 10,
    },
    itemDetails: {
        flex: 1,
    },
    itemName: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5,
        textAlign: 'center',
        margin: 10
    },
    itemDescription: {
        fontSize: 14,
        color: '#888',
        marginBottom: 5,
        borderLeftWidth: 2,
        paddingLeft: 5,
    },
    itemPrice: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'green',
        textAlign: 'right',
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    addButton: {
        marginTop: 10,
        alignSelf: 'stretch',
    },
    favoriteIcon: {
        position: 'absolute',
        top: 10,
        right: 10,
        zIndex: 1,
    },
});

export default ListItem;
