import React, { useState } from 'react';
import { Modal, SafeAreaView, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Searchbar } from 'react-native-paper';
import { Button as PaperButton } from 'react-native-paper';

const FilterModal = ({
    isModalVisible,
    setModalVisible,
    availableModels,
    setBrandFilter,
    applyFilters,
    availableBrands,
    setModelFilter,
    clearFilters
}) => {
    const [modelMenuVisible, setModelMenuVisible] = useState(false);
    const [brandMenuVisible, setBrandMenuVisible] = useState(false);
    const [filteredModels, setFilteredModels] = useState([]);
    const [filteredBrands, setFilteredBrands] = useState([]);
    const [searchModelText, setSearchModelText] = useState('');
    const [searchBrandText, setSearchBrandText] = useState('');

    const onModelInputChange = (text) => {
        setSearchModelText(text);
        filterModels(text);
        if (text) {
            setModelMenuVisible(true);
        } else {
            setModelMenuVisible(false);
        }
    };
    const onBrandInputChange = (text) => {
        setSearchBrandText(text);
        filterBrands(text);
        if (text) {
            setBrandMenuVisible(true);
        } else {
            setBrandMenuVisible(false);
        }
    };

    const filterModels = (text) => {
        if (text.length >= 1) {
            const filtered = availableModels.filter(model =>
                model.toLowerCase().includes(text.toLowerCase())
            );
            setFilteredModels(filtered);
        } else {
            setFilteredModels([]);
        }
    };

    const filterBrands = (text) => {
        if (text.length >= 1) {
            const filtered = availableBrands.filter(brand =>
                brand.toLowerCase().includes(text.toLowerCase())
            );
            setFilteredBrands(filtered);
        } else {
            setFilteredBrands([]);
        }
    };

    const onModelSelect = (model) => {
        setModelFilter(model);
        setSearchModelText(model);
        setModelMenuVisible(false);
    };

    const onBrandSelect = (brand) => {
        setBrandFilter(brand);
        setSearchBrandText(brand);
        setBrandMenuVisible(false);
    };

    const clearAll = () => {
        clearFilters();
        setSearchBrandText('');
        setSearchModelText('');
    }


    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={isModalVisible}
            onRequestClose={() => setModalVisible(false)}
        >
            <SafeAreaView style={{ flex: 1, paddingHorizontal: 16, margin: 10, marginTop: 30 }}>
                <PaperButton style={{ marginBottom: 10 }} icon="trash-can" mode="contained" onPress={() => clearAll()}>
                    Clear
                </PaperButton>
                <Searchbar
                    placeholder="Search Model"
                    value={searchModelText}
                    onChangeText={onModelInputChange}
                    style={{ marginBottom: 16 }}
                />
                {modelMenuVisible && filteredModels.map((model, index) => (
                    <TouchableOpacity style={styles.searchHelper} key={index} onPress={() => onModelSelect(model)} >
                        <Text>{model}</Text>
                    </TouchableOpacity>
                ))}
                <Searchbar
                    placeholder="Search Brand"
                    value={searchBrandText}
                    onChangeText={onBrandInputChange}
                    style={{ marginBottom: 16 }}
                />
                {brandMenuVisible && filteredBrands.map((brand, index) => (
                    <TouchableOpacity style={styles.searchHelper} key={index} onPress={() => onBrandSelect(brand)} >
                        <Text>{brand}</Text>
                    </TouchableOpacity>
                ))}

                <PaperButton icon="beaker-check" mode="contained" onPress={applyFilters}>
                    Apply Filters
                </PaperButton>
            </SafeAreaView>
        </Modal>
    );
};

const styles = StyleSheet.create({
    searchHelper: {
        margin: 2,
        alignSelf: 'center',
        fontSize: 29,
    },

});

export default FilterModal;
