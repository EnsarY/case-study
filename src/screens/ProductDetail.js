import React from 'react';
import { View, Text, Image, ScrollView, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native';
import { useDispatch } from 'react-redux';
import { addToCart } from '../slices/cartSlice';
import { showMessage } from "react-native-flash-message";

const ProductDetail = ({ route }) => {
    const { data } = route.params;
    const dispatch = useDispatch();

    const handleAddToCart = () => {
        dispatch(addToCart(data));
        showMessage({
            message: "Product added to cart",
            type: "success",
            position: 'center',
        });
    }
    return (
        <SafeAreaView style={styles.safeArea}>
            <ScrollView style={styles.container}>
                <Image source={{ uri: data.image }} style={styles.image} />
                <View style={styles.infoContainer}>
                    <Text style={styles.name}>{data.name}</Text>
                    <Text style={styles.brandModel}>{data.brand} - {data.model}</Text>
                    <Text style={styles.price}>${data.price}</Text>
                    <Text style={styles.createdAt}>Created At: {data.createdAt.split('T')[0]}</Text>
                    <Text style={styles.description}>{data.description}</Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={handleAddToCart}>
                    <Text style={styles.buttonText}>Add to Cart</Text>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: 300,
    },
    infoContainer: {
        padding: 20,
    },
    name: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    brandModel: {
        fontSize: 18,
        fontStyle: 'italic',
        marginBottom: 10,
    },
    price: {
        fontSize: 22,
        color: 'green',
        marginBottom: 10,
    },
    createdAt: {
        fontSize: 16,
        color: 'gray',
        marginBottom: 10,
    },
    description: {
        fontSize: 16,
        color: 'black',
        marginBottom: 20,
    },
    button: {
        backgroundColor: '#007bff',
        padding: 15,
        borderRadius: 5,
        alignItems: 'center',
        marginHorizontal: 20,
        marginBottom: 20,
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    }
});

export default ProductDetail;
