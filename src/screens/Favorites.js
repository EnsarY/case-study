import LottieView from 'lottie-react-native';
import React from 'react';
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { removeFromFavorites } from '../slices/favoritesSlice';

const Favorites = () => {
    const dispatch = useDispatch();
    const favorites = useSelector(state => state.favorites.items);

    const handleRemoveFromFavorites = (item) => {
        dispatch(removeFromFavorites(item));
    };

    const renderItem = ({ item }) => (
        <View style={styles.itemContainer}>
            <Image source={{ uri: item.image }} style={styles.itemImage} />
            <View style={styles.itemDetails}>
                <Text style={styles.itemName}>{item.name}</Text>
                <Text style={styles.itemPrice}>${item.price}</Text>
            </View>
            <TouchableOpacity testID='remove-from-favorites-button' onPress={() => handleRemoveFromFavorites(item)} style={styles.removeButton}>
                <MaterialCommunityIcons name="close-circle" size={24} color="red" />
            </TouchableOpacity>
        </View>
    );
    return (
        <SafeAreaView style={styles.container}>
            {favorites.length > 0 ? (
                <FlatList
                    data={favorites}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />
            ) : (
                <View style={styles.noFavorite}>
                    <LottieView style={{ width: 500, height: 500 }} source={require('../assets/lotie/empty-cart.json')} autoPlay loop />
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#fff',
    },
    itemContainer: {
        flexDirection: 'row',
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#f8f8f8',
        borderRadius: 10,
    },
    itemImage: {
        width: 80,
        height: 80,
        borderRadius: 40,
        marginRight: 10,
    },
    itemDetails: {
        justifyContent: 'center',
        flex: 1,
    },
    itemName: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    itemPrice: {
        fontSize: 16,
        color: 'green',
    },
    noFavoritesText: {
        fontSize: 18,
        color: 'gray',
        textAlign: 'center',
        marginTop: 20,
    },
    noFavorite: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    removeButton: {
        justifyContent: 'center',
        paddingLeft: 10,
    },
});

export default Favorites;
