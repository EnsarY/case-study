import React from 'react';
import { View, Text, FlatList, Image, Button, StyleSheet, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { removeFromCart, clearCart, addToCart } from '../slices/cartSlice';
import LottieView from 'lottie-react-native';

const Cart = () => {
    const dispatch = useDispatch();
    const cartItems = useSelector((state) => state.cart.items);
    const totalPrice = cartItems.reduce((total, item) => total + (item.price * item.quantity), 0);

    const handleRemoveFromCart = (id) => {
        dispatch(removeFromCart({ id }));
    };

    const handleClearCart = () => {
        dispatch(clearCart());
    };

    const handleAddToCart = (item) => {
        dispatch(addToCart({ id: item.id }));
    };

    const renderItem = ({ item }) => (
        <View style={styles.item}>
            <Image source={{ uri: item.image }} style={styles.itemImage} />
            <Text style={styles.itemName}>{item.name}</Text>
            <View style={styles.quantityContainer}>
                <Button title="-" onPress={() => handleRemoveFromCart(item.id)} />
                <Text style={styles.quantity}>{item.quantity}</Text>
                <Button title="+" onPress={() => handleAddToCart(item)} />
            </View>
            <Text style={styles.itemPrice}>${(item.price * item.quantity).toFixed(2)}</Text>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={cartItems}
                renderItem={renderItem}
                keyExtractor={item => item.id.toString()}
                ListFooterComponent={() => (
                    totalPrice > 0 && (
                        <View style={styles.footer}>
                            <Text style={styles.totalPrice}>Total: ${totalPrice.toFixed(2)}</Text>
                            <Button title="Clear Cart" onPress={handleClearCart} />
                        </View>
                    )
                )}
                ListEmptyComponent={() => (
                    <View style={styles.noCart}>
                        <LottieView style={{ width: '100%', height: 500 }} source={require('../assets/lotie/empty-cart2.json')} autoPlay loop />
                    </View>
                )}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
        padding: 10,
        backgroundColor: '#f8f8f8',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
    },
    itemImage: {
        width: 50,
        height: 50,
        marginRight: 10,
    },
    itemName: {
        flex: 1,
        fontWeight: 'bold',
    },
    itemPrice: {
        width: 60,
    },
    totalPrice: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    footer: {
        marginTop: 20,
        alignItems: 'center',
    },
    quantityContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    quantity: {
        marginHorizontal: 8,
    },
    emptyMessage: {
        textAlign: 'center',
        marginTop: 50,
        fontSize: 18,
    },
    noCart: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default Cart;
