import React, { useState, useEffect, useMemo } from 'react';
import { View, Text, StyleSheet, FlatList, SafeAreaView } from 'react-native';
import useFetchProducts from '../hooks/useFetchProducts';
import { ActivityIndicator, Searchbar, Button as PaperButton } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import FilterModal from '../components/FilterModal';
import ListItem from '../components/ListItem';
import styles from '../assets/css/styles';

const Home = () => {
    const { data, loading, error } = useFetchProducts('https://5fc9346b2af77700165ae514.mockapi.io/products');
    const [displayedData, setDisplayedData] = useState([]);
    const [isLoadingMore, setIsLoadingMore] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [modelFilter, setModelFilter] = useState('');
    const [brandFilter, setBrandFilter] = useState('');
    const [isFilterActive, setIsFilterActive] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');
    const [availableModels, setAvailableModels] = useState([]);
    const [availableBrands, setAvailableBrands] = useState([]);


    const ITEM_COUNT = 12;


    useEffect(() => {
        if (data && data.length > 0) {
            setDisplayedData(data.slice(0, ITEM_COUNT));
            const models = [...new Set(data.map(item => item.model))];
            setAvailableModels(models);
            const brands = [...new Set(data.map(item => item.brand))];
            setAvailableBrands(brands);
        }
    }, [data]);

    const filteredData = useMemo(() => {
        let result = data;

        const lowerCaseModelFilter = modelFilter.toLowerCase();
        const lowerCaseBrandFilter = brandFilter.toLowerCase();
        const lowerCaseSearchTerm = searchTerm.toLowerCase();

        if (modelFilter) {
            result = result.filter(item => item.model.toLowerCase().includes(lowerCaseModelFilter));
        }

        if (brandFilter) {
            result = result.filter(item => item.brand.toLowerCase().includes(lowerCaseBrandFilter));
        }

        if (searchTerm) {
            result = result.filter(item => item.name.toLowerCase().includes(lowerCaseSearchTerm));
        }


        return result;
    }, [data, modelFilter, brandFilter, searchTerm]);

    useEffect(() => {
        setDisplayedData(filteredData.slice(0, ITEM_COUNT));
        setIsFilterActive(modelFilter !== '' || brandFilter !== '' || searchTerm !== '');
    }, [filteredData]);


    if (loading) {
        return (
            <View style={styles.container}>
                <LottieView style={{ width: 500, height: 500 }} source={require('../assets/lotie/loading.json')} autoPlay loop />
            </View>
        )
    }

    if (error) {
        return <Text>Error: {error.message}</Text>;
    }

    const loadMoreData = () => {
        const nextIndex = displayedData.length;

        if (displayedData.length >= currentData.length) return;
        setIsLoadingMore(true);
        setTimeout(() => {
            const nextData = currentData.slice(nextIndex, nextIndex + ITEM_COUNT);
            setDisplayedData([...displayedData, ...nextData]);
        }, 1500);
    };


    const currentData = isFilterActive ? filteredData : data;

    const applyFilters = () => {
        let newFilteredData = data;

        if (modelFilter) {
            newFilteredData = newFilteredData.filter(item => item.model.includes(modelFilter));
        }

        if (brandFilter) {
            newFilteredData = newFilteredData.filter(item => item.brand.includes(brandFilter));
        }

        setDisplayedData(newFilteredData.slice(0, ITEM_COUNT));
        setIsFilterActive(true);
        setIsModalVisible(false);
    };

    const clearFilters = () => {
        setDisplayedData(data.slice(0, ITEM_COUNT));
        setModelFilter('');
        setBrandFilter('');
        setSearchTerm('');
        setIsFilterActive(false);
    };



    const renderFooter = () => {
        if (!isLoadingMore) return null;

        return (
            <View style={styles.footer}>
                <ActivityIndicator testID="loading-indicator" size="large" />
            </View>
        );
    };



    const renderItem = ({ item }) => (
        <View>
            <ListItem item={item} />

        </View>
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Searchbar
                placeholder="Search"
                onChangeText={setSearchTerm}
                value={searchTerm}
                style={page_styles.searchInput}
            />
            <View style={page_styles.filterButtons}>
                <PaperButton icon="filter" mode="contained" onPress={() => setIsModalVisible(true)}>
                    Open Filters
                </PaperButton>
                <PaperButton icon="trash-can" mode="contained" onPress={() => clearFilters()}>
                    Clear Filters
                </PaperButton>
            </View>


            <FilterModal
                isModalVisible={isModalVisible}
                setModalVisible={setIsModalVisible}
                modelFilter={modelFilter}
                setModelFilter={setModelFilter}
                brandFilter={brandFilter}
                setBrandFilter={setBrandFilter}
                applyFilters={applyFilters}
                ListFooterComponent={renderFooter}
                availableModels={availableModels}
                availableBrands={availableBrands}
                clearFilters={clearFilters}
            />

            <FlatList
                data={displayedData}
                renderItem={renderItem}
                keyExtractor={item => item.id.toString()}
                onEndReached={loadMoreData}
                onEndReachedThreshold={0.5}
                ListFooterComponent={displayedData.length < currentData.length ? renderFooter : null}
            />
        </SafeAreaView>
    );
};

const page_styles = StyleSheet.create({
    filterButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10,
    },
    searchInput: {
        margin: 10,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
    },
});

export default Home;
