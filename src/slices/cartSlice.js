import { createSlice } from '@reduxjs/toolkit';

export const cartSlice = createSlice({
    name: 'cart',
    initialState: {
        items: [],
    },
    reducers: {
        addToCart: (state, action) => {
            const existingIndex = state.items.findIndex((item) => item.id === action.payload.id);

            if (existingIndex >= 0) {
                state.items[existingIndex].quantity += 1;
            } else {
                const itemWithQuantity = { ...action.payload, quantity: 1 };
                state.items.push(itemWithQuantity);
            }
        },
        removeFromCart: (state, action) => {
            const index = state.items.findIndex(item => item.id === action.payload.id);
            if (index !== -1) {
                if (state.items[index].quantity > 1) {
                    state.items[index].quantity -= 1;
                } else {
                    state.items.splice(index, 1);
                }
            }
        },
        clearCart: (state) => {
            state.items = [];
        },

        setCartItems: (state, action) => {
            state.items = action.payload;
        },
    },
});

export const { addToCart, removeFromCart, clearCart, setCartItems } = cartSlice.actions;

export default cartSlice.reducer;
