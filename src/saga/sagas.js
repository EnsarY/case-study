import { call, put, takeEvery, select } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { cartSlice } from '../slices/cartSlice';

function* loadCartSaga() {
    try {
        const jsonValue = yield call(AsyncStorage.getItem, '@cart');
        const cartItems = jsonValue != null ? JSON.parse(jsonValue) : [];
        yield put(cartSlice.actions.setCartItems(cartItems));
    } catch (e) {
        console.log(e)

    }
}


function* saveCartSaga() {
    try {
        const cartItems = yield select((state) => state.cart.items);
        const jsonValue = JSON.stringify(cartItems);
        yield call(AsyncStorage.setItem, '@cart', jsonValue);
    } catch (e) {
        console.log(e)
    }
}

function* watchCartChanges() {
    yield takeEvery([
        cartSlice.actions.addToCart.type,
        cartSlice.actions.removeFromCart.type,
        cartSlice.actions.clearCart.type
    ], saveCartSaga);
}


export default function* rootSaga() {
    yield takeEvery("APP_INIT", loadCartSaga);
    yield watchCartChanges();
}
