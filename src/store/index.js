import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga/sagas';
import cartReducer from '../slices/cartSlice';
import favoritesReducer from '../slices/favoritesSlice';

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
    reducer: {
        cart: cartReducer,
        favorites: favoritesReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);
