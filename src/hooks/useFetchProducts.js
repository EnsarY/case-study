import { useState, useEffect } from 'react';
import axios from 'axios';

const useFetchProducts = (url) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    // yükleme animasyonunun gözükmesi için 1 saniye ekledim. Buradaki timerın sebebi sadece animasyonun gözükmesi için.
    useEffect(() => {
        const fetchData = async () => {
            try {
                setTimeout(async () => {
                    const response = await axios.get(url);
                    setData(response.data);
                    setLoading(false);
                }, 1000);
            } catch (error) {
                setError(error);
                setLoading(false);
            }
        };

        fetchData();
    }, [url]);

    return { data, loading, error };
};

export default useFetchProducts;
