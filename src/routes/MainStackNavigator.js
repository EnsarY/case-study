import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TabNavigator from './TabNavigator';
import ProductDetail from '../screens/ProductDetail';
import { useDispatch } from 'react-redux';

const MainStack = createStackNavigator();

const MainStackNavigator = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch({ type: 'APP_INIT' });
    }, [dispatch]);

    return (
        <MainStack.Navigator>
            <MainStack.Screen
                name="Tabs"
                component={TabNavigator}
                options={{ headerShown: false }}

            />

            <MainStack.Screen options={{
                headerBackTitle: 'Back',
                headerTitle: 'Product Detail'
            }} name="Detail" component={ProductDetail} />
        </MainStack.Navigator>
    );
}



export default MainStackNavigator;