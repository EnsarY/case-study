import React from 'react';
import { useSelector } from 'react-redux';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from '../screens/Home';
import Cart from '../screens/Cart';
import Favorites from '../screens/Favorites';

const Tab = createMaterialBottomTabNavigator();

const TabNavigator = () => {
    const cartItems = useSelector((state) => state.cart.items);
    const cartItemCount = cartItems.reduce((total, item) => total + item.quantity, 0);


    return (
        <Tab.Navigator
            barStyle={{ backgroundColor: 'white' }}
        >
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, focused }) => (
                        <MaterialCommunityIcons name={focused ? "home" : "home-outline"} color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Favorites"
                component={Favorites}
                options={{
                    tabBarLabel: 'Favorites',
                    tabBarIcon: ({ color, focused }) => (
                        <MaterialCommunityIcons name={focused ? "star" : "star-outline"} color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Cart"
                component={Cart}
                options={{
                    tabBarLabel: 'Cart',
                    tabBarBadge: cartItemCount > 0 ? cartItemCount : null, // Sepette ürün yoksa badge gösterme
                    tabBarIcon: ({ color, focused }) => (
                        <MaterialCommunityIcons name={focused ? "cart" : "cart-outline"} color={color} size={26} />
                    ),
                }}
            />
        </Tab.Navigator>
    );

};

export default TabNavigator;
