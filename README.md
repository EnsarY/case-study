Api isteğinde 1 saniyelik timer koydum bunun sebebi animasyonun gözükmesi için. Normal bir mantıkta böyle bir şey kullanmıyorum.

Projede kod karmaşası yaratacak yerlerde örneğin Home.js komponentlere ayırdım. Küçük sayfalar için bunu yapmadım (cart,favorites,detail). 

Genel kullanılacaak stilleri assets klasörü altında style dosyasında topluyorum. Sayfa özelinde olan stilleri sayfa içinde kullandım.

Projeyi best practices kurallarına uygun şekilde kodlamaya çalıştım. Filtrelemeler için debounce kullanılabilirdi ve genel olarak proptypesları da kullanabilirdim. Bunları eklemedim.

Favoriler sadece redux içinde çalışır. Sepetteki ürünler ise device storage'da saklanıyor.
