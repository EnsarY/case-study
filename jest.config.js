module.exports = {
  preset: 'react-native',
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transformIgnorePatterns: [
    'node_modules/(?!(react-native'
    + '|react-native-vector-icons'
    + '|react-native-paper'
    + '|react-native-flash-message'
    + '|@react-native-community'
    + '|react-navigation'
    + '|@react-navigation/.*'
    + '|lottie-react-native'
    + '|@react-native'
    + '|react-redux'
    + '|redux'
    + '|redux-saga'
    + '|@react-navigation'
    + '|@react-native-community'
    + '|react-native-iphone-screen-helper'
    + '|react-native-flash-message'
    + '|react-native-vector-icons'
    + '|@react-navigation'
    + '|@react-native-community'
    + '|@react-native-async-storage/async-storage'

    + ')/)',
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|webp|svg)$': '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js'
  },

};
