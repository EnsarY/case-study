import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import ProductDetail from '../src/screens/ProductDetail';

const mockStore = configureMockStore();
const store = mockStore({});

const route = {
    params: {
        data: {
            id: 1,
            name: 'Test Product',
            brand: 'Test Brand',
            model: 'Test Model',
            price: 100,
            image: 'http://example.com/product.jpg',
            description: 'Test Description',
            createdAt: '2021-01-01T12:00:00.000Z'
        }
    }
};

jest.mock("react-native-flash-message", () => ({
    showMessage: jest.fn(),
}));

describe('ProductDetail Component', () => {
    it('renders product details correctly', () => {
        const { getByText, getByTestId } = render(
            <Provider store={store}>
                <ProductDetail route={route} />
            </Provider>
        );

        expect(getByText('Test Product')).toBeTruthy();
        expect(getByText('Test Brand - Test Model')).toBeTruthy();
        expect(getByText('$100')).toBeTruthy();
        expect(getByText('Test Description')).toBeTruthy();
        expect(getByText('Created At: 2021-01-01')).toBeTruthy();
    });

    it('dispatches add to cart action when button pressed', () => {
        const { getByText } = render(
            <Provider store={store}>
                <ProductDetail route={route} />
            </Provider>
        );

        const addToCartButton = getByText('Add to Cart');
        fireEvent.press(addToCartButton);
        expect(store.getActions()).toContainEqual({
            type: 'cart/addToCart',
            payload: route.params.data
        });
    });
});
