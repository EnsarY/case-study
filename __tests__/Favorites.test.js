import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import Favorites from '../src/screens/Favorites';

const mockStore = configureMockStore();
const store = mockStore({
    favorites: {
        items: [
            { id: 1, name: 'Favorite Product 1', price: 100, image: 'http://example.com/product1.jpg' },
        ],
    },
});

jest.mock('react-native-vector-icons/MaterialCommunityIcons', () => 'MaterialCommunityIcons');

describe('Favorites Component', () => {
    it('renders favorite items correctly', () => {
        const { getByText } = render(
            <Provider store={store}>
                <Favorites />
            </Provider>
        );

        expect(getByText('Favorite Product 1')).toBeTruthy();
    });


    it('removes an item from favorites', () => {
        const { getAllByTestId } = render(
            <Provider store={store}>
                <Favorites />
            </Provider>
        );

        const removeButtons = getAllByTestId('remove-from-favorites-button');
        fireEvent.press(removeButtons[0]);

        expect(store.getActions()).toContainEqual({
            type: 'favorites/removeFromFavorites',
            payload: {
                id: 1,
                name: 'Favorite Product 1',
                price: 100,
                image: 'http://example.com/product1.jpg'
            }
        });
    });

});
