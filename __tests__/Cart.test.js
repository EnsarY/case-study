import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import MockStorage from '../__mocks__/mockStorage';
import Cart from '../src/screens/Cart';


const storageCache = {};
const AsyncStorage = new MockStorage(storageCache);

jest.setMock('AsyncStorage', AsyncStorage)
const mockStore = configureMockStore();
const store = mockStore({
    cart: {
        items: [
            { id: 1, name: 'Product 1', price: 10.00, quantity: 1, image: 'http://example.com/product1.jpg' },
            { id: 2, name: 'Product 2', price: 20.00, quantity: 2, image: 'http://example.com/product2.jpg' }
        ],
    },
});



jest.mock('lottie-react-native', () => 'LottieView');

describe('Cart Component', () => {
    it('renders cart items correctly', () => {
        const { getByText, getAllByText } = render(
            <Provider store={store}>
                <Cart />
            </Provider>
        );

        expect(getByText('Product 1')).toBeTruthy();
        expect(getByText('Product 2')).toBeTruthy();

        const totalPrice = 10.00 * 1 + 20.00 * 2;
        expect(getByText(`Total: $${totalPrice.toFixed(2)}`)).toBeTruthy();
    });

    it('removes an item from the cart', () => {
        const { getAllByText } = render(
            <Provider store={store}>
                <Cart />
            </Provider>
        );

        const firstMinusButton = getAllByText('-')[0];
        fireEvent.press(firstMinusButton);
        expect(store.getActions()).toContainEqual({ type: 'cart/removeFromCart', payload: { id: 1 } });
    });

    it('clears the cart', () => {
        const { getByText } = render(
            <Provider store={store}>
                <Cart />
            </Provider>
        );

        fireEvent.press(getByText('Clear Cart'));
        expect(store.getActions()).toContainEqual({ type: 'cart/clearCart' });
    });
});
