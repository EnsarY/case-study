import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider} from 'react-redux';
import { store } from './src/store';
import MainStackNavigator from './src/routes/MainStackNavigator';
import FlashMessage from "react-native-flash-message";




function App() {


  return (
    <StoreProvider store={store}>
      <PaperProvider>
        <NavigationContainer>
          <MainStackNavigator />
        </NavigationContainer>
        <FlashMessage position="top" />
      </PaperProvider>
    </StoreProvider>
  );
}

export default App;
